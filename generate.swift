#!/usr/bin/env swift

import Foundation

let hosturl = "https://gitlab.com/ArMuSebastian/themer/-/raw/main/"
let fm = FileManager.default
let initialPath = fm.currentDirectoryPath

let pwdUrl = URL(string: initialPath)!
let pwdString = pwdUrl.absoluteString

let jsonEncoder = JSONEncoder()
jsonEncoder.outputFormatting = [.withoutEscapingSlashes, .prettyPrinted]

let pwdUrls = try! fm.contentsOfDirectory(
    at: pwdUrl,
    includingPropertiesForKeys: nil,
    options: .skipsHiddenFiles
)

let themesUrls = try! fm.contentsOfDirectory(
    at: pwdUrl.appendingPathComponent("Themes"),
    includingPropertiesForKeys: [URLResourceKey.isDirectoryKey],
    options: .skipsHiddenFiles
)

func createLoadingMapFile(with urls: [URL]) -> Data {

    struct Config: Codable {
        var themes: [ThemeInfo]
        var themeMap: [ResourceGroup]
    }

    struct ResourceGroup: Codable {
        var group: String
        var items: [Resource]
    }

    struct Resource: Codable {
        var name: String
    }

    struct ThemeInfo: Codable {
        var name: String
        var map: String
    }

    let groups = [
        ResourceGroup.init(
            group: "images",
            items: [
                Resource.init(name: "image1")
            ]
        ),
        ResourceGroup.init(
            group: "colors",
            items: [
                Resource.init(name: "color1"),
                Resource.init(name: "color2"),
                Resource.init(name: "color3")
            ]
        )
    ]

    let themes = urls
        .map { url in
            let name = url.lastPathComponent
            let path = try! fm.contentsOfDirectory(
                at: url,
                includingPropertiesForKeys: nil,
                options: .skipsHiddenFiles
            )
                .first { $0.lastPathComponent.hasSuffix("json") }!
                .path

            let relativeString = String(path.dropFirst(pwdString.count))

            return ThemeInfo.init(name: name, map: hosturl + relativeString)
    }
    let config = Config.init(themes: themes, themeMap: groups)
    
    return try! jsonEncoder.encode(config)
}

func createMainLoadingMapFile() {
    let data = createLoadingMapFile(with: themesUrls)
    let file = pwdUrls.first { $0.lastPathComponent.hasSuffix("json") }!
    try! data.write(to: file)
}

createMainLoadingMapFile()

func createThemeMap(from url: URL) {
    
    struct Pair {
        var assosiatedName: String
        var url: String
    }
    
    struct ThemeMap: Codable {
        var themeMap: [String: String]
    }
    
    let themesContentUrls = try! fm.contentsOfDirectory(
        at: url,
        includingPropertiesForKeys: [URLResourceKey.isDirectoryKey],
        options: .skipsHiddenFiles
    )
    let r = themesContentUrls
        .filter { !$0.lastPathComponent.hasSuffix("json") }
        .flatMap { new -> [Pair] in
            try! fm.contentsOfDirectory(
                at: new,
                includingPropertiesForKeys: [URLResourceKey.isDirectoryKey],
                options: .skipsHiddenFiles
            )
            .map { (fileUrl: URL) -> Pair in
                    Pair.init(
                        assosiatedName: String(new.lastPathComponent + "." + String(fileUrl.lastPathComponent.split(separator: ".")[0])),
                        url: String(fileUrl.path.dropFirst(pwdString.count))
                    )
                }
    }
        .reduce(into: [String: String]()) { accumulated, new in
            accumulated[new.assosiatedName] = hosturl + new.url
        }
    
    let config = ThemeMap.init(themeMap: r)

    let data = try! jsonEncoder.encode(config)
    let file = themesContentUrls.first { $0.lastPathComponent.hasSuffix("json") }!
    try! data.write(to: file)
}

for themeUrl in themesUrls {
    createThemeMap(from: themeUrl)
}
